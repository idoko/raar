package algo

// start at beginning of array, for each item you encounter, if it's > the next element,
// swap it with the next element. when you're done, biggest item should be last
// in the next iteration, end your search at n-i since the last i elements would have being presumably
// sorted.

//Bubble sorts the input array using bubble sort
func Bubble(arr []int) []int {
	wall := len(arr)

	for i := 0; i < wall; i++ {
		if arr[i] > arr[i+1] {
			arr[i], arr[i+1] = arr[i+1], arr[i]
		}
		wall = len(arr) - i
	}
	return arr
}
