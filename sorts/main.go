package main

import (
	"fmt"

	"gitlab.com/idoqo/raar/sorts/algo"
)

func main() {
	n := []int{14, 33, 27, 35, 10}
	s := algo.Bubble(n)
	fmt.Println("Before: ", n)
	fmt.Println("After: ", s)
}
